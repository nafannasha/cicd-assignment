const express = require("express");
const app = express();
const port = 4048;

app.get('/',(req,res) =>{
    res.send("Hello World")
});
app.listen(port,()=>{
    console.log("App is running on port 4048")
});